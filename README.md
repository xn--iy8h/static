##Hello World! (README.md)

You have a some html on a git repository and want to serve it as a page ..
however you want to avoid the *jekyll dance* of the git-pages !

still github, gitlab, bitbucket, gitea serve your ran js and html as "text/plain"
and you might have some CORS restriction too !

Well you have a solution now to have some gitpage w/o [CI] on IPFS !

Please note that this page site is static, while it display the latest content
of the master branch [README.md][1] on the gitlab repository [.../static/](https://gitlab.com/xn--iy8h/static.git).
It is directly served from the git repository via [ipfs]
(w/o the need of [CI/CD][ci]).

Each page load is triggering a fetch of the [raw](https://gitlab.com/xn--iy8h/static/-/raw/master/README.md) page
from the repository,
so be careful and don't abuse it !
(if you have high volume requirement use the proper [CDN]...)

### how does it work ...

The raw page is fetched via an "[ipfs urlstore][2] add" therefore it appears on
the IPFS network, hence removing the need to deal
with the wrong _Content-Type_ and restrictive _CORS_ !

The hash is then used to retrieve the data to replace the &lt;html> tag within the
original document.

*voilà* !




[ipfs]: https://dist.ipfs.io/
[ci]: https://qwant.com/?q=CI/CD
[CDN]: https://statically.io/
[1]: https://gitlab.com/xn--iy8h/static/-/blob/master/README.md
[2]: https://docs.ipfs.io/reference/http/api/#api-v0-urlstore-add
